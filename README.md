# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Detailing the Ultrasonic sensor and how we ended up using it.
### How do I get set up? ###

Instructions should be in the wiki, if unclear follow reference link. Also we used a 4pin sensor instead of a 3pin sensor so setups may be different.

### Contribution guidelines ###

https://learn.parallax.com/tutorials/language/propeller-c/propeller-c-simple-devices/sense-distance-ping 
https://create.arduino.cc/projecthub/microBob/ultra-sonic-ping-sensor-a9c49e
### Who do I talk to? ###

Christopher Hoang